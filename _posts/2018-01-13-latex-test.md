---
layout: post
title: "LaTex test"
uuid: 26b8af3b-61a2-4741-8639-6bf41b3997e6
date: 2018-01-13 01:31:52 +0800
tags: [latex]
categories: EN
mathjax: true
comments: true
---
This is test of mathjax

$$
f'\left( x\right) = \lim _{x\rightarrow 0}\dfrac {f\left( x+\Delta x\right) - f\left( x\right)}{\Delta x}
$$
