#!/bin/bash

################################
# Source:
#   allenyllee/jekyllpostgen: A Jekyll Post Generator for BASH
#       https://github.com/allenyllee/jekyllpostgen
#
# Usage:
#       ./jekyllpostgen.sh "title Abc" -c "category" -t "tag,foo,bar" -mn
#   -m: enable mathjax
#   -n: enable comments
################################

# call jekyllpostgen.sh
../../jekyllpostgen/jekyllpostgen.sh "$@"